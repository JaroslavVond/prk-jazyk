import model.Operand;
import model.Vars;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Listener extends ExprBaseListener {

    Stack<Operand> stack = new Stack<>();
    List<Vars> vars = new ArrayList<>();

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        super.exitEveryRule(ctx);
    }

    @Override
    public void exitAdd(ExprParser.AddContext ctx) {
        Operand x = stack.pop();
        Operand y = stack.pop();
        Double xDouble = Calculation.getDoubleFromStr(x.getNumber());
        Double yDouble = Calculation.getDoubleFromStr(y.getNumber());

        if(xDouble != null && yDouble != null){
            double a = Math.round((xDouble + yDouble) * 100);
            a = a / 100;
            stack.push(new Operand(String.valueOf(a), "DOUBLE"));
        }
        else{
            System.err.println("not valid data type");
        }
        super.exitAdd(ctx);
    }

    @Override
    public void exitMul(ExprParser.MulContext ctx) {
        Operand x = stack.pop();
        Operand y = stack.pop();
        Double xDouble = Calculation.getDoubleFromStr(x.getNumber());
        Double yDouble = Calculation.getDoubleFromStr(y.getNumber());

        if(xDouble != null && yDouble != null){
            double a = Math.round((xDouble * yDouble) * 100);
            a = a / 100;
            stack.push(new Operand(String.valueOf(a), "DOUBLE"));
        }
        else{
            System.err.println("not valid data type");
        }

        super.exitMul(ctx);
    }

    @Override
    public void exitKom(ExprParser.KomContext ctx) {
        String k = stack.pop().getNumber();
        String n = stack.pop().getNumber();
        if(Calculation.checkIfInt(n) && Calculation.checkIfInt(k)){
            int intN = (int) Double.parseDouble(n);
            int intK = (int) Double.parseDouble(k);
            if(intK <= intN && intK > 0 && intN > 0)
                stack.push(new Operand(String.valueOf(Math.round(Calculation.variation(intN, intK) * 100)/100), "UINT"));
            else
                System.err.println("K musí být <= N a N,K > 0");
        }
        else {
            System.err.println("Výpočet variací bez opakování lze pouze pro celá kladná čísla.");
        }

        super.exitKom(ctx);
    }

    @Override
    public void exitSub(ExprParser.SubContext ctx) {
        String n = stack.pop().getNumber();
        if(Calculation.checkIfInt(n)){
            int intN = (int) Double.parseDouble(n);
            if(intN > 0)
                stack.push(new Operand(String.valueOf(Math.round(Calculation.subfactorial(intN)*100)/100), "UINT"));
            else
                System.err.println("N musí být > 0");
        }
        else {
            System.err.println("Subfaktorial lze vypočítat pouze pro celá kladná čísla.");
        }
        super.exitSub(ctx);
    }

    @Override
    public void exitCom(ExprParser.ComContext ctx) {
        Operand x = stack.pop();
        Operand y = stack.pop();
        Double xDouble = Calculation.getDoubleFromStr(x.getNumber());
        Double yDouble = Calculation.getDoubleFromStr(y.getNumber());
        if(yDouble > xDouble)
            stack.push(new Operand("1", "BOOL"));
        else
            stack.push(new Operand("0", "BOOL"));
        super.exitCom(ctx);
    }

    @Override
    public void exitUint(ExprParser.UintContext ctx) {
        stack.push(new Operand(ctx.getText(), "UINT"));
        super.exitUint(ctx);
    }

    @Override
    public void exitInt(ExprParser.IntContext ctx) {
        stack.push(new Operand(ctx.getText(), "INT"));
        super.exitInt(ctx);
    }

    @Override
    public void exitDouble(ExprParser.DoubleContext ctx) {
        stack.push(new Operand(ctx.getText(), "DOUBLE"));
        super.exitDouble(ctx);
    }

    @Override
    public void exitBool(ExprParser.BoolContext ctx) {
        stack.push(new Operand(ctx.getText(), "BOOL"));
        super.exitBool(ctx);
    }

    @Override
    public void exitByte(ExprParser.ByteContext ctx) {
        stack.push(new Operand(ctx.getText(), "BYTE"));
        super.exitByte(ctx);
    }


    @Override
    public void exitInitVar(ExprParser.InitVarContext ctx) {
        vars.add(new Vars(ctx.getChild(1).getText(), ctx.getChild(3).getText(), ctx.getChild(0).getText()));
        super.exitInitVar(ctx);
    }

    @Override
    public void exitVar(ExprParser.VarContext ctx) {
        for (Vars var : vars){
            if(var.getName().equals(ctx.getText())){
                stack.push(new Operand(var.getNum(), var.getType()));
            }
        }
        super.exitVar(ctx);
    }

    @Override
    public void visitErrorNode(ErrorNode node) {
        System.out.println("...ERROR Terminal visit: " + node.getText());
        super.visitErrorNode(node);
    }
}
