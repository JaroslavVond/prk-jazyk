package model;

public class Vars {
    String name;
    String num;
    String type;

    public Vars(String name, String num, String type) {
        this.name = name;
        this.num = num;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getNum() {
        return num;
    }

    public String getType() {
        return type;
    }
}
