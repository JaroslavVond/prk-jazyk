package model;

public class Operand {

    String number;
    String type;

    public Operand(String number, String type) {
        this.number = number;
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }
}
