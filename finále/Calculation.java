public class Calculation {

    public static double variation(int n, int k){
        return factorial(n) / (n - k);
    }

    private static double factorial(int n){
        if (n == 0)
            return 1;
        else
            return(n * factorial(n-1));
    }

    public static double subfactorial(int n){
        double subFact = 0;
        for(int i = 2; i <= n; i++){
            subFact += Math.pow(-1, i) / factorial(i);
        }
        subFact = subFact * factorial(n);
        return subFact;
    }

    public static boolean checkIfInt(String x) {
        if(Double.parseDouble(x) % 1 == 0)
            return true;

        try {
            Integer.parseInt(x);
        } catch (Exception ex) {
            return false;
        }

        return false;
    }

    public static double getDoubleFromStr(String num){
        try{
            return Double.parseDouble(num);
        }catch (Exception e){
            System.err.println("Error while parsing string to double: " + e.getMessage());
            return Double.parseDouble(null);
        }
    }
}
