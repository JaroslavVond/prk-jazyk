import model.Operand;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println("Compiling...");

        ExprLexer lexer = new ExprLexer(CharStreams.fromFileName("example.txt"));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ExprParser parser = new ExprParser(tokens);

        ParseTree tree = parser.start();

        //System.out.println(tree.toStringTree(parser));

        Listener extractor = new Listener();
        ParseTreeWalker.DEFAULT.walk(extractor, tree); // initiate walk of tree with listener in use of default walker

        for(Operand o : extractor.stack){
            System.out.println(o.getNumber());
        }

        System.out.println("Complete");
    }

}
