grammar Expr;		

MUL:  '*';
ADD:  '+';
KOMB: '~';

WHITESPACE : (' ' | '\t') -> skip;
BOOL    : BOOL_NUMBER ;
BYTE    : UNSIGNED_INTEGER+ ;
UINT    : NUMBER ;
INT     : SIGN? NUMBER+ ;
DOUBLE  : SIGN? NUMBER+ (DOT UNSIGNED_INTEGER+)? ;
NEWLINE : [\r\n\t]+ ;
VAR: [a-zA-Z_]+ ;

start:	(expr NEWLINE)* ;
expr:	initVar # Var
    |   expr ('*') expr # Mul
    |   expr ('~') expr # Kom
    |	expr ('+') expr # Add
    |	expr ('>') expr # Com
    |	'|'expr'|' # Zav
    |	'('expr')' # Zav2
    |   'subfactorial(' expr ')' # Sub
    |   BOOL       # Bool
    |   BYTE       # Byte
    |   UINT       # Uint     
    |	INT        # Int
    |   DOUBLE     # Double
    |   VAR        # Var
    |   dataType   # dataTypef
    ;

variable : VAR ;
dataValue : UINT | INT | BYTE | DOUBLE | BOOL;
dataType
    : '_uint'
    | '_int'
    | '_byte'
    | '_double'
    | '_bool'
    ;

initVar : dataType variable '=' dataValue ;

fragment UNSIGNED_INTEGER : [0-9]+ ;
fragment NUMBER : [0-9] ;
fragment SIGN : ('&+' | '&-') ;
fragment BOOL_NUMBER: [0-1] ;
fragment DOT: ('.') ;