grammar Expr;		
start:	(expr NEWLINE)* ;
expr:	expr ('*') expr
    |   expr ('~') expr
    |	expr ('+') expr
    |	'|'expr'|'
    |	'('expr')'
    |   UINT
    |	INT
    |   BYTE
    |   DOUBLE
    |   BOOL
    ;
    
WHITESPACE : (' ' | '\t') -> skip;
UINT    : NUMBER ;
INT     : SIGN? NUMBER+ ;
BYTE    : UNSIGNED_INTEGER+ ;
DOUBLE  : SIGN? NUMBER+ (DOT UNSIGNED_INTEGER+)? ;
BOOL    : BOOL_NUMBER ;
NEWLINE : [\r\n]+ ;


fragment UNSIGNED_INTEGER : ('0' .. '9')+ ;
fragment NUMBER : ('0' .. '9') ;
fragment SIGN : ('&+' | '&-') ;
fragment BOOL_NUMBER: ('0' .. '1') ;
fragment DOT: ('.') ;
