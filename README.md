## Datové typy
- byte
- int (32 bit)
- double

## Operace
- Výchozí operace: sčítání +, násobení *, závorky ()
- Přidané operace: porovnání velikosti >, Variace bez opakování ~     výraz: n~k pro k,n ∈ ℕ, k <= n
- Závorky: ||

## Funkce

- Výpočet subfaktoriálů pro n < 170

## Příklady výrazů

```sh
|(a+a)*(a~a)| // výstup: ℝ
|(a+a)*(a~a)| > (a+a) // výstup: true|false
```

## Příklady použití

```sh
5 > 1 // výstup: true
10 > 11 // výstup: false
6 > 6 // výstup: false
```

```sh
8 ~ 2 // výstup: 56
2 ~ 8 // výstup: -1
```

```sh
subfactorial(5) // výstup: 44
subfactorial(171) // výstup: inf
```
## GRAMATIKA
```sh
grammar Expr;		
start:	(expr NEWLINE)* ;
expr:	expr ('*'|'~') expr
    |	expr ('+') expr
    |	INT
    |   BYTE
    |   DOUBLE
    |   BOOL
    |	'|' expr '|'
    |	'(' expr ')'
    ;
NEWLINE : [\r\n]+ ;
INT     : (SIGN)? NUMBER ;
BYTE    : UNSIGNED_INTEGER ;
DOUBLE  : (SIGN)? NUMBER+ (DOT UNSIGNED_INTEGER+)? ;
BOOL    : BOOL_NUMBER ;


fragment UNSIGNED_INTEGER : ('0' .. '9')+ ;
fragment NUMBER : ('0' .. '9')
fragment SIGN : ('+' | '-') ;
fragment BOOL_NUMBER: ('0' .. '1')
```
