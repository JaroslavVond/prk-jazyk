grammar Expr;		
start:	(expr NEWLINE)* ;
expr:	expr'*'expr
    |   expr'~'expr
    |	expr (PLUS) expr
    |	INT
    |   BYTE
    |   DOUBLE
    |   BOOL
    |	'|'expr'|'
    |	'('expr')'
    ;
WHITESPACE : (' ' | '\t') -> skip;
NEWLINE : [\r\n]+ ;
INT     : (SIGN)? NUMBER ;
BYTE    : UNSIGNED_INTEGER ;
DOUBLE  : (SIGN)? NUMBER+ (DOT UNSIGNED_INTEGER+)? ;
BOOL    : BOOL_NUMBER ;
PLUS    : '+' ;


fragment UNSIGNED_INTEGER : ('0' .. '9')+ ;
fragment NUMBER : ('0' .. '9') ;
fragment SIGN : ('+' | '-') ;
fragment BOOL_NUMBER: ('0' .. '1') ;
fragment DOT: ('.') ;
